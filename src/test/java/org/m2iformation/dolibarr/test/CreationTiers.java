package org.m2iformation.dolibarr.test;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreationTiers {
	
	public static ChromeDriver driver;

	@BeforeClass
	public static void setupClass() throws Exception {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
		driver.quit();
	}

	@Before
	public void setUp() throws Exception {
		driver.get("http://demo.testlogiciel.pro/dolibarr/");
		driver.findElement(By.id("username")).sendKeys("jsmith");
		driver.findElement(By.name("password")).sendKeys("dolibarrhp");
		driver.findElement(By.xpath("//input[contains(@value,'Connexion')]")).click();
	}

	@After
	public void tearDown() throws Exception {
		driver.findElement(By.xpath("//img[@alt='D�connexion']")).click();
	}

	@Test
	public void testCreationTiers() {
		driver.findElement(By.id("mainmenua_companies")).click();
		driver.findElement(By.className("vsmenu")).click();
		assertTrue(driver.findElement(By.id("radiocompany")).isSelected());
		new Select(driver.findElement(By.name("client"))).selectByVisibleText("Client");
		new Select(driver.findElement(By.id("fournisseur"))).selectByVisibleText("Non");
		assertTrue(driver.findElement(By.name("code_client")).getAttribute("value").matches("CU[0-9]{4}-[0-9]{4}"));
		driver.findElement(By.name("nom")).sendKeys(getRandomReference());
		String varClient = driver.findElement(By.name("nom")).getAttribute("value");
		driver.findElement(By.xpath("//input[contains(@value,'Cr�er tiers')]")).click();
		for(int i=0; i<3; i++) {
			driver.findElement(By.className("addnewrecord")).click();
			driver.findElement(By.name("lastname")).sendKeys(getRandomReference());
			driver.findElement(By.name("add")).click();
		}
		
	}
	
	public String getRandomReference(){
		 String ref="";
		 String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWYZ";
		 int stringLength = 4;
		 for (int i=0; i<stringLength; i++) {
		        int rnum = (int) Math.floor(Math.random() * allowedChars.length());
		        ref += allowedChars.substring(rnum,rnum+1);
		    }
		    ref += "-";
			for (int i=0; i<stringLength; i++) {
		        int rnum = (int) Math.floor(Math.random() * 9);
		        ref += rnum;
		    }
		 return ref;
	 }


}
